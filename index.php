<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration Form</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <form action="result.php" method="post" onsubmit="return validateForm()">
        <div class="h1-frame">
            <h1>Registration</h1>
        </div>

        <!-- User name input -->
        <label for="username">User Name:</label>
        <input type="text" name="username" id="username">
        <div id="usernameError" class="error-message"></div>

        <!-- Email input -->
        <label for="email">Email Address:</label>
        <input type="email" name="email" id="email">
        <div id="emailError" class="error-message"></div>

        <!-- Date of birth input -->
        <label for="dob">Date of Birth:</label>
        <input type="date" name="dob" id="dob" max="<?php echo date('Y-m-d'); ?>">
        <div id="dobError" class="error-message"></div>

        <!-- Message textarea -->
        <label for="message">Message (Optional):</label>
        <textarea name="message" id="message" rows="4"></textarea>

        <button type="submit">Submit</button>
    </form>

    <script>
        function validateForm() {
            // Simple validation for demonstration purposes
            var username = document.getElementById("username").value;
            var email = document.getElementById("email").value;
            var dob = document.getElementById("dob").value;
            var message = document.getElementById("message").value;

            // Clear existing error messages
            clearErrorMessages();

            var isValid = true;

            if (!username) {
                displayErrorMessage("usernameError", "Username is required.");
                isValid = false;
            }

            if (!email) {
                displayErrorMessage("emailError", "Email is required.");
                isValid = false;
            }

            if (!dob) {
                displayErrorMessage("dobError", "Date of birth is required.");
                isValid = false;
            }

            if (!isValid) {
                return false;
            }

            return true; // Form will be submitted if validation passes
        }

        function displayErrorMessage(elementId, message) {
            var errorElement = document.getElementById(elementId);
            errorElement.innerText = message;
            errorElement.style.color = "red";
            errorElement.style.fontSize = "smaller";
            errorElement.style.fontWeight = "bold"
            errorElement.style.marginBottom = "10px";
        }

        function clearErrorMessages() {
            var errorElements = document.querySelectorAll(".error-message");
            errorElements.forEach(function (element) {
                element.innerText = "";
            });
        }
    </script>
</body>
</html>
