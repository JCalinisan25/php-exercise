# PHP Exercise



## Description

A registration form for users to enter their data. On form submit, the users will be redirected to another page that will reflect the data they entered in the previous page.

## Project status

User name > accept all text input only; case insensitive ✅

Email address > accept only email type input ✅
- If input is not correct email format, make it color red on the next page ✅
- Correct email format should be the following: some.string@domain.com ✅

Date of birth > accept only date input ✅
- On the next page output “you are a minor” if user’s age is below 18. Else, output “you are not a minor” ✅
- Do not accept date input after today’s date ✅

Message > accept any input type; case insensitive ✅
- On form submit, remove all special characters from the user input (example: !&*#$) and output the new - - message content ✅

## Visuals
Registration Form
![Alt text](img/image-2.png)

User Input
![Alt text](img/image-3.png)

Result
![Alt text](img/image-4.png)

No User Input
![Alt text](img/image-5.png)

Email Wrong Format
![Alt text](img/image-6.png)