<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $username = $_POST["username"];
        $email = $_POST["email"];
        $dob = $_POST["dob"];
        $message = $_POST["message"];

        // Check if the email is in the correct format
        $emailPattern = '/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}$/';
        $isEmailValid = preg_match($emailPattern, $email);

        // Calculate age based on the entered date of birth
        $today = new DateTime();
        $birthDate = new DateTime($dob);
        $age = $today->diff($birthDate)->y;

        // Remove special characters from the message
        $cleanMessage = preg_replace('/[^A-Za-z0-9 ]/', '', $message);

        // Display the results
        echo "<!DOCTYPE html>";
        echo "<html lang='en'>";
        echo "<head>";
        echo "<meta charset='UTF-8'>";
        echo "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
        echo "<title>User Information</title>";
        echo "<link rel='stylesheet' href='style.css'>";
        echo "</head>";
        echo "<body>";
        echo "<div class='result-frame'>";
        echo "<h2>User Information</h2>";
        echo "<p><b>Username:</b> $username</p>";
        echo "<p><b>Email Address:</b> " . ($isEmailValid ? $email : "<span class='error'>$email</span>") . "</p>";
        echo "<p><b>Date of Birth:</b> $dob";

        if ($age < 18) {
            echo " <span class='minor'>(You are a minor)</span>";
        } else {
            echo " <span class='adult'>(You are not a minor)</span>";
        }

        echo "</p>";
        echo "<p><b>Message:</b> $cleanMessage</p>";
        echo "</div>";
        echo "</body>";
        echo "</html>";
    }
?>
